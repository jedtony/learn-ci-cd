import React, { Suspense, lazy } from "react";
// import Login from './components/authentication/Login'
import { BrowserRouter, Route, Switch } from "react-router-dom";
const Login = lazy(() => import("./components/authentication/Login"));

// ================ LEADS ===================== 
const CreateLead = lazy(() => import('./components/pages/leads/CreateLead'));

function App() {
  const baseUrl = process.env.PUBLIC_URL;
  return (
    <BrowserRouter basename={baseUrl}>
      <Suspense fallback={<div>Loading...</div>}>
        <Switch>
          <Route path="/" exact>
            <Login />
          </Route>

          <Route path="/lead/create" exact>
            <CreateLead />
          </Route>
          
        </Switch>
      </Suspense>
    </BrowserRouter>
  );
}

export default App;
