export const getUserName = () => {
    return localStorage.getItem('CN_ERP_USERNAME');
}

export const getUserId = () => {
    return localStorage.getItem('CN_ERP_L_USER_ID');
}

export const setUserName = (name) => {
    localStorage.setItem('CN_ERP_USERNAME', name);
}

export const setUserId = (id) => {
    localStorage.setItem('CN_ERP_USER_ID', id)
}

export const setToken = (token) => {
    localStorage.setItem('CN_ERP_TOKEN', token);
}

export const getToken = () => {
   return localStorage.getItem('CN_ERP_TOKEN');
}