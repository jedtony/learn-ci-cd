import moment from 'moment';

export function getCurrentDate() {
    return moment.now();
}