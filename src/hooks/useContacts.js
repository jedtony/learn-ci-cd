import {useState} from 'react'

export default function useContacts() {
  const [contacts, setContacts] = useState([
      {label: 'MTN', value: 'MTN'},
      {label: 'Glo', value: 'Glow'},
      {label: 'Airtel', value: 'Airtel'}
  ])

  return contacts
}
