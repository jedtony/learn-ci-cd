import useCategories from './useCategories';
import useCompanies from './useCompanies';
import useChannels from './useChannels';
import useMedium from './useMedium';
import useProducts from './useProducts';
import useContacts from './useContacts';
import useCountries from './useCountries';
import {useCountryStates } from './useCountries'
import useIndustries from './useIndustries';
import useGender from './useGender';
 
export {useCategories, useCompanies, useChannels, 
    useMedium, useProducts, useContacts, useCountries, 
useCountryStates, useIndustries, useGender}