import {useState} from 'react'

export default function useIndustries() {
    const [categories, setCategories] = useState([
        { value: 'B2B', label : 'B2B'},
        {label: 'B2C', value: 'B2C'},
        {label: 'B2B2C', value: 'B2B2C'}
         ])

         return categories;
}
