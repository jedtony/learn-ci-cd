import {useState} from 'react'

export default function useChannel() {
  const [channels, setchannels] = useState([
      {label: 'MTN', value: 'MTN'},
      {label: 'Glo', value: 'Glow'},
      {label: 'Airtel', value: 'Airtel'}
  ])

  return channels
}