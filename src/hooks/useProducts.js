import {useState} from 'react'

export default function useProducts() {
  const [products, setproducts] = useState([
      {label: 'MTN', value: 'MTN'},
      {label: 'Glo', value: 'Glow'},
      {label: 'Airtel', value: 'Airtel'}
  ])

  return products
}