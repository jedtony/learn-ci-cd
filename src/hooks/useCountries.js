import {useState, useEffect} from 'react'

export default function useCountries() {
  const [channels, setchannels] = useState([
      {label: 'MTN', value: 'MTN'},
      {label: 'Glo', value: 'Glow'},
      {label: 'Airtel', value: 'Airtel'}
  ])

  return channels
}

export function useCountryStates(refetch) {
    const [countryStates, setCountryStates] = useState([
        {label: 'MTN', value: 'MTN'},
        {label: 'Glo', value: 'Glow'},
        {label: 'Airtel', value: 'Airtel'}
    ])

    useEffect(() => {
       
    }, [refetch])
    return countryStates;
}