import {useState} from 'react'

export default function useCompanies() {
  const [companies, setCompanies] = useState([
      {label: 'MTN', value: 'MTN'},
      {label: 'Glo', value: 'Glow'},
      {label: 'Airtel', value: 'Airtel'}
  ])

  return companies
}
