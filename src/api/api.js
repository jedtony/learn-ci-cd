import axios from 'axios';
import * as localStore from '../helpers/localStorageHelper';

const VERSION_ONE = 'v1';
const VERSION_TWO = 'v2';

const apiUrl = 'http://157.245.37.253/erp/public/api/v1/';

function init() {
    const headers = {
      Accept: "application/json",
      "Content-Type": "application/json",
      // "Content-Type": "multipart/form-data",
      // 'Authorization': `Bearer ${this.getToken()}`
    }
    const instance = axios.create({
        baseURL: apiUrl,
        timeout: 1000,
        headers
      });

      instance.interceptors.response.use(function (response) {
        response = { ...response, status: 200, statusCode: 200 };
        return response;
    }, function (error) {
        if (error.response) {
            
            let status = error.response.status;
            if (status === 401) {
                // redirectToLogin();
            }
        }
        let customError = Promise.reject(error)
        return {error, statusCode: 500};
        // return { error: 'Unable to connect to the internet', statusCode: 500 }

    })

      return instance;


}

    // To process results 
   function processResult(response, version) {
      console.log('I am logging the status code');
      let {statusCode} = response;
      
      console.log(response);
      if(statusCode === 200) {
          let success = null;

          if(version === VERSION_ONE){
          success = response.data.success
      } else {
          success = response.data.data
      }
          return {statusCode, success};
      }
       else if(statusCode === 201) {
          return {statusCode, success: response.data}
      } else {
          let error = 'Cannot connect to the internet';
          return {statusCode, error}
      }

  }

  
function postLogin(data) {
//   let results = await init().post('login', data);
//   return processResult(results, 'v1');
    // let results = await axios.post('http://157.245.37.253/erp/public/api/login', data);

    // console.log(results.statusCode);
    // let {status} = results;
    
    // if(status === 200) {
    //     localStore.setToken(results.data.success.token);
    // }

   return axios.post('http://157.245.37.253/erp/public/api/login', data)
    .then(res=> {
        let {success} = res.data;
        localStore.setToken(success.token);
        localStore.setUserId(success.info.id);
        localStore.setUserName(success.info.name);
        return {statusCode: 200, data: 'Success'}
    }).catch(error => {
        
        if(error.response) {
           if( error.response.status == 401) {
               return {statusCode: 401, data: "Email and password does not match any record"}
           }
           else {
            return {statusCode: 500, data: "Error in internet connection"} 
           }
        }else {
            console.log('an error ', error)
            return {statusCode: 500, data: "Error in internet connection"}
    }

  });
  }

export default {postLogin}