import React, {useState} from 'react'
import {Drawer, PageTitle} from '../../units/molecules';
import {ModalButton as CustomButton} from '../../units/atoms'
import { Form, Row, Input, Button, Select, Divider } from 'antd';
import 'antd/dist/antd.css';
import {useCategories, useCompanies, useChannels, 
  useMedium, useProducts, useContacts} from '../../../hooks'

import {CreateCompany, CreateContact, CreateMedium, CreateChannel, Modal} from '../../units/molecules/modals'
import {FormGroup} from '../../units/utils';

const { Option } = Select;


export default function CreateLead(props) {
  const categories = useCategories();
  const companies = useCompanies();
  const channels = useChannels();
  const medium = useMedium();
  const products = useProducts();
  const contacts = useContacts();
  const assigns = useContacts ();

  const [companyModal, setCompanyModal] = useState(false);

  const [channelModal, setChannnelModal] = useState(false);

  const [mediumModal, setMediumModal] = useState(false);

  const [contactModal, setContactModal] = useState(false);


  const { TextArea } = Input;

  const defaultSelectWidth = 300;

    const breadcrumbs = [
        {'label': 'Dashboard'},
        {'label' : 'Create Lead'}
    ]

    const onFinish = values => {
      console.log(values)
    }
    return (
        <Drawer>
            <PageTitle title="Create Lead" breadcrumbs={breadcrumbs} />

 {/* =========== COMPANY MODAL  ============*/}
<Modal 
 visible={companyModal}
 title="New Company"
//  onOk={this.handleOk}
 onCancel={setCompanyModal}
 >
<CreateCompany />
</Modal>

{/*============== CONTACT MODAL ============*/}
<Modal 
 visible={contactModal}
 title="New Contact"
//  onOk={this.handleOk}
 onCancel={setContactModal}
 >
<CreateContact />
</Modal>

{/*============== MEDIUM MODAL ============*/}
<Modal 
 visible={mediumModal}
 title="New Medium"
//  onOk={this.handleOk}
 onCancel={setMediumModal}
 >
<CreateMedium />
</Modal>

{/*============== MEDIUM MODAL ============*/}
<Modal 
 visible={channelModal}
 title="New Channel"
//  onOk={this.handleOk}
 onCancel={setChannnelModal}
 >
<CreateChannel />
</Modal>



            <Form
      // {...layout}
      layout="vertical"
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      size={'large'}
      // onFinishFailed={onFinishFailed}
    >

            <Row gutter={24}>
          <FormGroup 
            name={'category'}
            label={'Category'}
            required= {true}>
              <Select  style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  categories.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
            </FormGroup>


            <FormGroup 
            name={'company'}
            label={'Company'}
            required= {true}>
              
          <Select
          showSearch
          style={{ width: defaultSelectWidth }}
          // placeholder="Select a company"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
              >
                {
                  companies.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select>   
    <CustomButton label="New Company" onClick={()=> setCompanyModal(!companyModal)} />
            </FormGroup> 

                </Row>


                <Row gutter={24}>

            <FormGroup 
            name={'channel'}
            label={'Channel'}
            required= {true}>
              
          <Select
          showSearch
          style={{ width: defaultSelectWidth }}
          // placeholder="Select a Channel"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
              >
                {
                  channels.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select>

    <CustomButton label="New Channel" onClick={()=> setChannnelModal(!channelModal)} />
            </FormGroup>
     
            <FormGroup 
            name={'medium'}
            label={'Medium'}
            required= {true}>
              
          <Select
          showSearch
          style={{ width: defaultSelectWidth }}
          // placeholder="Select a Medium"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
              >
                {
                  medium.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
    <CustomButton label="New Medium" onClick={()=> setMediumModal(!mediumModal)} />
            </FormGroup>
    
    </Row>

    <Row gutter={24}>
            <FormGroup 
            name={'product'}
            label={'Product'}
            required= {true}>
              
          <Select
          showSearch
          style={{ width: defaultSelectWidth }}
          // placeholder="Select a Product"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
              >
                {
                  products.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select>
                
            </FormGroup>
    
          
            <FormGroup 
            name={'contact'}
            label={'Contacts'}
            required= {true}>
              
          <Select
          showSearch
          style={{ width: defaultSelectWidth }}
          // placeholder="Select a Contact"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
              >
                {
                  contacts.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select>
    <CustomButton label="New Contact" onClick={()=> setContactModal(!contactModal)} />
            </FormGroup>
    
    </Row>

    <Row gutter={24}>
          
            <FormGroup 
            name={'assign'}
            label={'Assign'}
            required= {true}>
              
          <Select
          showSearch
          style={{ width: defaultSelectWidth }}
          // placeholder="Select a Assign"
          optionFilterProp="children"
          filterOption={(input, option) =>
            option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
          }
        >
              >
                {
                  assigns.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select>
            </FormGroup>
</Row>
<Row gutter={24}>

            <Divider orientation="left">More Information</Divider>
            <FormGroup 
            name={'reason'}
            label={'Reason For Enquiry'}
            required= {true}>
              <TextArea placeholder="Their reason for needing this product is..." allowClear
              //  onChange={onChange} 
              style={{width: '500px', height: '100px'}}
               />
            </FormGroup>

            <FormGroup 
            name={'backgroundInfo'}
            label={'Background Information'}
            required= {true}>
              <TextArea placeholder="Brief info about the company..." allowClear
              //  onChange={onChange} 
              style={{width: '500px', height: '100px'}}
               />
            </FormGroup>
    
          
            </Row>

            <Form.Item >
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>

        </Form>
      
        </Drawer>
    )
}

