import React, {useState} from 'react';
import 'antd/dist/antd.css';
import {Form, Row, Input, Select} from 'antd';
import {FormGroup, FullFormGroup} from '../../utils'
import {useCountries, useCountryStates, useIndustries} from '../../../../hooks'
import {ModalButton} from '../../atoms'
import CreateTagline from './CreateTagline';

import {Modal} from '../modals'

export default function CreateCompany() {

    const countries = useCountries();
    let countryStates = useCountryStates();
    const industries = useIndustries();

    const [taglineModal, setTaglineModal] = useState(false);

    const onFinish = values => {
        console.log(values)
      }

      const { Option } = Select;
      const defaultSelectWidth = 220;

    return (
        <div>

<Modal 
 visible={taglineModal}
 title="New Tagline"
//  onOk={this.handleOk}
 onCancel={setTaglineModal}>
 
<CreateTagline />
</Modal>
   
     
                  <Form
    //   {...layout}
      layout="vertical"
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      size={'large'}
      // onFinishFailed={onFinishFailed}
    >
        <Row >
        <FullFormGroup 
            name={'name'}
            label={'Company Name'}
            required= {true} >
           <Input />
            </FullFormGroup>
        </Row>

        <Row gutter={4}>
        <FormGroup 
            name={'email'}
            label={'Company Email'}
            required= {true}>
           <Input />
            </FormGroup>

            <FormGroup 
            name={'phone'}
            label={'Phone Number'}
            required= {true}>
           <Input />
            </FormGroup>
        </Row>

        <Row gutter={4}>
        <FullFormGroup 
            name={'address'}
            label={'Address'}
            required= {true}>
           <Input style={{width: '100%'}} />
            </FullFormGroup>
        </Row>


        <Row gutter={4}>
        <FormGroup 
            name={'country'}
            label={'Country'}
            required= {true}>
             <Select   style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  countries.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
            </FormGroup>

            <FormGroup 
            name={'countryState'}
            label={'State'}
            required= {true}>
            <Select   style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  countryStates.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
            </FormGroup>

            
        </Row>
        
        <Row gutter={4}>
        <FormGroup 
            name={'city'}
            label={'City'}
            required= {true}>
           <Input />
            </FormGroup>

            <FormGroup 
            name={'website'}
            label={'Company Website'}
            required= {true}>
           <Input />
            </FormGroup>
        </Row>

        <Row gutter={4}>
        <FormGroup 
            name={'industries'}
            label={'Tagline'}
            required= {true}>
            <Select   style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  industries.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
    <ModalButton label="New Tagline" onClick={() => setTaglineModal(!taglineModal)} />
            </FormGroup>
        </Row>
        </Form> 


        </div>
    )
}
