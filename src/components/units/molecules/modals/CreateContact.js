import React from 'react'
import PropTypes from 'prop-types';
import {Form, Row, Input, Select, DatePicker, Divider} from 'antd';
import {FormGroup, FullFormGroup} from '../../utils'
import {useCountries, useCountryStates, useCompanies, useGender} from '../../../../hooks'

import {getCurrentDate} from '../../../../helpers';
import moment from 'moment';

const { Option } = Select;
const { TextArea } = Input;
 
function CreateContact(props) {

    const countries = useCountries();
    let countryStates = useCountryStates();
    const gender = useGender();
    const companies = useCompanies();

    const defaultSelectWidth = 220;
    const onFinish = values => {
        console.log(values)
      }


    return (
        <div>
            <Form
    //   {...layout}
      layout="vertical"
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      size={'large'}
      // onFinishFailed={onFinishFailed}
    >
        <Row >
        <FullFormGroup 
            name={'name'}
            label={'Full Name'}
            required= {true} >
           <Input />
            </FullFormGroup>
        </Row>

        <Row gutter={4}>
        <FormGroup 
            name={'email'}
            label={'Company Email'}
            required= {true}>
           <Input />
            </FormGroup>

            <FormGroup 
            name={'phone'}
            label={'Phone Number'}
            required= {true}>
           <Input />
            </FormGroup>
        </Row>

        <Row gutter={4}>
        <FullFormGroup 
            name={'address'}
            label={'Address'}
            required= {true}>
           <Input style={{width: '100%'}} />
            </FullFormGroup>
        </Row>

        <Row gutter={4}>
            <FormGroup 
            name={'gender'}
            label={'Gender'}
            required= {true}>
           <Select   style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  gender.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
            </FormGroup>

            <FormGroup 
            name={'city'}
            label={'City'}
            required= {true}>
           <Input />
            </FormGroup>
        </Row>



        <Row gutter={4}>
        <FormGroup 
            name={'country'}
            label={'Country'}
            required= {true}>
             <Select   style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  countries.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
            </FormGroup>

            <FormGroup 
            name={'countryState'}
            label={'State'}
            required= {true}>
            <Select   style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  countryStates.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
            </FormGroup>

            
        </Row>
        
       
       <Row gutter={4}>
       <FormGroup 
            name={'date'}
            label={'Date'}
            required= {true}
            >
           <DatePicker defaultValue={moment(new Date)} />
            </FormGroup>

            <FormGroup 
            name={'externalLink'}
            label={'External Link'}
            required= {false}
            >
           <Input />
            </FormGroup>
       </Row>

       <Row gutter={4}>
       <Divider orientation="left">More Information</Divider>

            <FormGroup 
            name={'position'}
            label={'Position'}
            required= {true}>
           <Input />
            </FormGroup>


            <FormGroup 
            name={'company'}
            label={'Company'}
            >
           <Select   style={{ width: defaultSelectWidth }} 
              // onChange={handleChange}
              >
                {
                  companies.map(cat => (<Option value={cat.value}>{cat.label}</Option>))
                }
    </Select> 
            </FormGroup>
        </Row>
       
        <Row gutter={24}>
        <FullFormGroup 
            name={'additionalInfo'}
            label={'Additional Information'}
            required= {true}>
              <TextArea placeholder="Brief info about the company..." allowClear
              //  onChange={onChange} 
              style={{ height: '100px'}}
               />
            </FullFormGroup>
            </Row>
    </Form>
        </div>
    )
}

CreateContact.propTypes = {

}

export default CreateContact

