import CreateCompany from '../modals/CreateCompany';
import Modal from './Modal'
import CreateContact from './CreateContact';
import CreateMedium from './CreateMedium';
import CreateChannel from './CreateChannel';

export {CreateCompany, Modal, CreateContact,
     CreateMedium, CreateChannel}