import React, {useState} from 'react'
import 'antd/dist/antd.css';
import './drawer.css';
import { Layout, Menu, Typography } from 'antd';
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
  VideoCameraOutlined,
  UnorderedListOutlined,
  UploadOutlined,
} from '@ant-design/icons';

const { Header, Sider, Content } = Layout;

const { SubMenu } = Menu;

function Drawer(props) {
    const [collapse, setCollapse] = useState(false);

    const toggle = () => {
        setCollapse(!collapse);
    }

    return (
        <Layout style={{height: 'inherit'}}>
        <Sider trigger={null} collapsible collapsed={collapse}>
          <div className="logo" />
          <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
          <SubMenu key="sub1"
          //  icon={<MailOutlined />}
            title="Lead Management">
            <Menu.Item key="5">Create Lead</Menu.Item>
            <Menu.Item key="6">View Lead</Menu.Item>
           
          </SubMenu>
 
            <Menu.Item key="sales" icon={<UnorderedListOutlined />}>
              Sales Management
            </Menu.Item>
            <Menu.Item key="2" icon={<VideoCameraOutlined />}>
              nav 2
            </Menu.Item>
            <Menu.Item key="3" icon={<UploadOutlined />}>
              nav 3
            </Menu.Item>
          </Menu>
        </Sider>
        <Layout className="site-layout">
          <Header className="site-layout-background" style={{ padding: 0 }}>
            {React.createElement(collapse ? MenuUnfoldOutlined : MenuFoldOutlined, {
              className: 'trigger',
              onClick: toggle,
            })}
            {/* <Title level={4}>Concept Nova ERP</Title> */}
            {/* <strong>Concept Nova ERP</strong> */}
            Concept Nova ERP
          </Header>
          <Content
            className="site-layout-background"
            style={{
              margin: '24px 16px',
              padding: 24,
              minHeight: 280,
            }}
          >
            {props.children}
          </Content>
        </Layout>
      </Layout>
    
    )
}

Drawer.propTypes = {

}

export default Drawer

