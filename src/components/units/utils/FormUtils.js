import React from 'react';
import { Form, Row, Col, Input, Button, Select, Divider, Modal } from 'antd';
import 'antd/dist/antd.css';

export function FormGroup({name, label, required, ...props}) {
    return (

        <Col span={{xs: 24, md: 12, xl: 8}} >
        <Form.Item
          name={name}
          label={label}
          rules={ required? [
            {
              required: {required},
              message: 'This field is required',
            },
          ] : []}
        >
        {props.children}
        </Form.Item>
      </Col>
 
    )
}

export function FullFormGroup({name, label, required, ...props}) {

 


  return (

    <Col style={{width: '100%'}} >
    <Form.Item
      name={name}
      label={label}
      // {...layout}
      rules={[
        {
          required: {required},
          message: 'This field is required',
        },
      ]}
      // style={{minWidth: '480px'}}
    >
    {props.children}
    </Form.Item>
  </Col>

)
}