import React from 'react';
import {Button} from 'antd'

export default function ({onClick, label}) {
    return(
  <Button type="primary" size="default" onClick={onClick}>
           {label}
          </Button>
    )
  }
