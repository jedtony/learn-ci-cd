import React, {useState} from 'react'
import 'antd/dist/antd.css';
import { Row, Col, Form, Input, Button, Checkbox, Alert } from 'antd';
import './login.css';
import {useApi} from '../../context'
import '../../constants/constantStyles.css'


function Login() {

  let api = useApi();

  const [isFailed, setIsFailed] = useState(false);
  const [message, setMessage] = useState('');


    const layout = {
        labelCol: {
          span: 8,
        },
        wrapperCol: {
          span: 16,
        },
      };
      const tailLayout = {
        wrapperCol: {
          offset: 8,
          span: 16,
        },
      };

      const marginBottom = {
        marginBottom: '20px'
      }

    const onFinish = values => {
      setIsFailed(false)
        console.log('Success:', values);
        
        let data = {
          email: values.email,
          password: values.password
        }

        const postForm = async() => {
          let results = await api.postLogin(data);

          // console.log(results);
          let {statusCode} = results;

          if(statusCode === 200){

          }
          else {
            let {data} = results;
            setMessage(data)
            setIsFailed(true)
          }
        }

        postForm();

      };

    // Executed when the form validation fails
      const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
      };

    return (
        <Row justify="space-around" align="middle" className="lightBackground">
      {/* <Col span={{xs: 24, md: 24, xl: 24}}> */}
      <Col span={8}>
       {isFailed &&  (<Row style={marginBottom}>
          <Col span={24} >
      <Alert 
      message="Error"
      description={message}
      type="error"
      showIcon
    />
    </Col>
    </Row>)
}
      <Form
      {...layout}
      name="basic"
      initialValues={{
        remember: true,
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item style={{marginLeft: '0px'}}
        label="Email"
        name="email"
        // onChange={(value) => console.log(value.target.value)}
        // onValuesChange={(value)=> console.log('logging the value ' , value)}
        rules={[
          {
            required: true,
            message: 'Please input your email!',
          },
        ]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[
          {
            required: true,
            message: 'Please input your password!',
          },
        ]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item {...tailLayout} name="remember" valuePropName="checked">
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  
      </Col>
      
      
    </Row>
    )
}

Login.propTypes = {

}


export default Login

